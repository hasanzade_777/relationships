package com.example.relationships.service;

import com.example.relationships.dto.AddressDto;
import com.example.relationships.model.*;

import javax.management.relation.Role;
import java.util.List;
import java.util.Optional;

public interface RelationshipService {
    void addUser(User user);
    void addUser(Registration register);
    List<Registration> getReg();
    void addAdressToUser(Address address,Long id);
    void deleteAddress(Long id);
    void deleteContact(Long id);
    void deleteRole(Long id);
    void addRole(UserRole role);
    void addRoleToUser(UserRole role,Long id);
    List<User> getUserAndAdress();
    List<AddressDto> getAddress();
}
