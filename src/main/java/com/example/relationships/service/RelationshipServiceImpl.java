package com.example.relationships.service;

import com.example.relationships.dto.AddressDto;
import com.example.relationships.map.AddressMap;
import com.example.relationships.model.Address;
import com.example.relationships.model.Registration;
import com.example.relationships.model.User;
import com.example.relationships.model.UserRole;
import com.example.relationships.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RelationshipServiceImpl implements RelationshipService {

    private final UsersRepository usersRepository;
    private final AddressesRepository addressesRepository;
    private final StudentRepository studentRepository;
    private final CourseRepository courseRepository;
    private final RegistrationRepository registrationRepository;
    private final UserRoleRepository userRoleRepository;
    private final ContactRepository contactRepository;
    private final AddressMap addressMap;

    @Override
    public void addUser(User user) {
        usersRepository.save(user);
    }

    @Override
    public void addAdressToUser(Address address, Long id) {
        User user = usersRepository.findById(id).orElseThrow(RuntimeException::new);
        address.getContact().forEach(contact -> contact.setAddresses(address));
        user.setAddress(address);
        usersRepository.save(user);
    }

    @Override
    public void deleteAddress(Long id) {
        addressesRepository.deleteById(id);
    }

    @Override
    public void deleteContact(Long id) {
        contactRepository.deleteById(id);
    }

    @Override
    public void deleteRole(Long id) {
        userRoleRepository.deleteById(id);
    }

    @Override
    public void addRole(UserRole role) {
        userRoleRepository.save(role);
    }

    @Override
    public void addRoleToUser(UserRole role, Long id) {
        UserRole role1 = userRoleRepository.findByRole(role.getRole()).orElseThrow(RuntimeException::new);
        User user = usersRepository.findById(id).get();
        user.getRole().add(role1);
        usersRepository.save(user);
    }

    @Override
    public List<User> getUserAndAdress() {
        return usersRepository.findAll();
    }

    @Override
    public List<AddressDto> getAddress() {
        return  addressMap.mapToDto(addressesRepository.finded());

    }


    @Override
    public void addUser(Registration register) {
        var course = courseRepository.findById(register.getCourse().getId());
        course.ifPresent(value -> register.setCourse(courseRepository.getById(value.getId())));
        register.setRegistered_at(LocalDateTime.now());
        registrationRepository.save(register);
    }

    @Override
    public List<Registration> getReg() {
        return registrationRepository.findAll();
    }
}
