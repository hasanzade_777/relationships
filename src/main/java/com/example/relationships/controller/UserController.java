package com.example.relationships.controller;

import com.example.relationships.dto.AddressDto;
import com.example.relationships.model.Address;
import com.example.relationships.model.Registration;
import com.example.relationships.model.User;
import com.example.relationships.model.UserRole;
import com.example.relationships.service.RelationshipServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/relationships")
@RequiredArgsConstructor
public class UserController { //UserController
    private final RelationshipServiceImpl service;

    @PostMapping("/users")
    public void addUsers(@RequestBody User user) {
        service.addUser(user);
    }

    @GetMapping
    public List<User> getUsers() {
        return service.getUserAndAdress();
    }

    @PostMapping("/user/{id}")
    public void addAddress(@RequestBody Address address, @PathVariable Long id) {
        service.addAdressToUser(address, id);
    }
    @PostMapping("/role")
    public void createRole(@RequestBody UserRole role) {
        service.addRole(role);
    }
    @GetMapping("/address")
    public List<AddressDto> getAddressList(){
        return service.getAddress();
    }
    @PostMapping("/role/{id}")
    public void addRole(@RequestBody UserRole role, @PathVariable Long id) {
        service.addRoleToUser(role, id);
    }
    @DeleteMapping("/role/{id}")
    public void deleteRole(@PathVariable Long id){
        service.deleteRole(id);
    }
    @DeleteMapping("/address/{id}")
    public void deleteAddress(@PathVariable Long id){
        service.deleteAddress(id);
    }
    @DeleteMapping("/contact/{id}")
    public void deleteContact(@PathVariable Long id){ service.deleteContact(id);}
    @PostMapping("/registration")
    public void addRegister(@RequestBody Registration register) {
        service.addUser(register);
    }

    @GetMapping("/registration")
    public List<Registration> getRegister() {
        return service.getReg();
    }

}
