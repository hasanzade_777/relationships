package com.example.relationships.repository;

import com.example.relationships.model.Roles;
import com.example.relationships.model.UserRole;
import java.util.Optional;
import javax.swing.text.html.Option;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    Optional<UserRole> findByRole(Roles rule);

    @Query(nativeQuery = true, value = "select * from user_role u where u.role = :rule")
    Optional<UserRole> findByRoleSql(Roles rule);
}