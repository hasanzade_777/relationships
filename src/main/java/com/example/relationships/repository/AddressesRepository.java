package com.example.relationships.repository;

import com.example.relationships.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AddressesRepository extends JpaRepository<Address, Long> {
    //@Query(value = "select * from address as unvan inner join contact c on unvan.id=c.address_id_1_1",nativeQuery = true)
    @Query(value = "select a from Address as a join fetch a.contact")
    List<Address> finded();
}