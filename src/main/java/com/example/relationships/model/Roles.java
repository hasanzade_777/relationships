package com.example.relationships.model;

public enum Roles {
    ADMIN,
    SUPER_ADMIN,
    USER,
    VISITOR
}
