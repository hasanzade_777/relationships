package com.example.relationships.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.persistence.*;
import lombok.ToString;

@Entity
@Data
@RequiredArgsConstructor
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String contactNumber;

    @JsonIgnore
    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "address_id_1_1")
    Address addresses;
}
